#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "md5.h"

int main(int argc, char *args[]) {
    if (argc == 3) {
        
        FILE *inputFile = fopen(args[1], "r");
        if (inputFile==NULL) {
            printf("%s is not a valid file.\n", args[1]);
            exit(1);
        } 
        
        FILE *outputFile = fopen(args[2], "w");
        if (outputFile==NULL) {
            printf("%s is not a valid file.\n", args[2]);
            exit(1);
        }
        
        char line[1000];
        while (fgets(line, 1000, inputFile) != NULL) {
            char *hash = md5(line, strlen(line));
            fprintf(outputFile, "%s\n", hash);
        }
        
        fclose(inputFile);
        fclose(outputFile);
    } else {
        printf("Invalid Syntax. %d\n",argc);
    }
}